# Introduction
Your task is to create a simple Microblogging API using Node.js and Express.js. This API will allow users to create, read, update, and delete blog posts. It should also allow users to comment on blog posts.

## Requirements

### Technology Stack
- Node.js
- Express.js
- Storage: In memory

### Features

**Create a Blog Post**
- Input: A JSON object containing 'title' and 'content' fields.
- Action: Create a new blog post.
- Response: Return the newly created blog post with a unique post ID.

**Get All Blog Posts**
- Action: Retrieve all blog posts.
- Response: A list of all blog posts with details (ID, title, content, comments).

**Get a Specific Blog Post**
- Action: Retrieve a specific blog post using its ID.
- Response: Details of the specified blog post including its ID, title, content, and comments.

**Update a Blog Post**
- Input: A JSON object containing 'title' and/or 'content' fields.
- Action: Update the specified blog post with the new title and/or content.
- Response: The updated blog post.

**Delete a Blog Post**
- Action: Delete the specified blog post.
- Response: A message indicating the result of the action (e.g., "Blog post deleted successfully").

**Add a Comment to a Blog Post**
- Input: A JSON object containing a 'comment' field.
- Action: Add a new comment to the specified blog post.
- Response: The updated blog post with the new comment.

### Evaluation Criteria
- Functionality: Does the API work as expected?
- Code Quality: Is the code well-organized, modular, and easy to comprehend?
- Error Handling: How does the application deal with potential errors or unexpected inputs?

### Bonus (Only if there is time)
- Create a basic react UI to retrieve and display blog posts